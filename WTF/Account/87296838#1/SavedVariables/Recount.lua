
RecountDB = {
	["profileKeys"] = {
		["Xazuk - Stormscale"] = "Xazuk - Stormscale",
		["Kuzax - Stormscale"] = "Kuzax - Stormscale",
	},
	["profiles"] = {
		["Xazuk - Stormscale"] = {
			["MainWindow"] = {
				["Position"] = {
					["y"] = -198.819840053711,
					["h"] = 161.000045776367,
					["w"] = 312.000091552734,
					["x"] = 516.439979302086,
				},
				["Buttons"] = {
					["FileButton"] = false,
					["CloseButton"] = false,
					["LeftButton"] = false,
					["ResetButton"] = false,
					["ConfigButton"] = false,
					["RightButton"] = false,
				},
				["BarText"] = {
					["NumFormat"] = 3,
				},
			},
			["ReportLines"] = 3,
			["MainWindowHeight"] = 160.555572509766,
			["Colors"] = {
				["Other Windows"] = {
					["Background"] = {
						["b"] = 0,
						["g"] = 0,
						["r"] = 0,
					},
					["Title"] = {
						["r"] = 0,
					},
				},
				["Window"] = {
					["Background"] = {
						["b"] = 0,
						["g"] = 0,
						["r"] = 0,
					},
					["Title"] = {
						["r"] = 0,
					},
				},
				["Class"] = {
					["MONK"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 1,
						["b"] = 0.59,
					},
					["SHAMAN"] = {
						["b"] = 0.87,
						["g"] = 0.44,
						["r"] = 0,
					},
				},
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
			},
			["DetailWindowY"] = -169.032943725586,
			["DetailWindowX"] = 326.303100585938,
			["Scaling"] = 1.08,
			["MainWindowVis"] = false,
			["LastInstanceName"] = "Proving Grounds",
			["BarTexture"] = "Armory",
			["RealtimeWindows"] = {
				["Realtime_!RAID_DAMAGE"] = {
					"!RAID", -- [1]
					"DAMAGE", -- [2]
					"Raid DPS", -- [3]
					-488, -- [4]
					189.000061035156, -- [5]
					199.999923706055, -- [6]
					232.000061035156, -- [7]
					false, -- [8]
				},
				["Realtime_Bandwidth Available_AVAILABLE_BANDWIDTH"] = {
					"Bandwidth Available", -- [1]
					"AVAILABLE_BANDWIDTH", -- [2]
					"", -- [3]
					-379.999903731343, -- [4]
					103.999904645279, -- [5]
					199.999938964844, -- [6]
					232.000045776367, -- [7]
					false, -- [8]
				},
			},
			["CurDataSet"] = "OverallData",
			["BarTextColorSwap"] = false,
			["MainWindowMode"] = 5,
			["MainWindowWidth"] = 312.036926269531,
		},
		["Kuzax - Stormscale"] = {
			["MainWindowVis"] = false,
			["MainWindow"] = {
				["Position"] = {
					["y"] = -149.580108900433,
					["h"] = 128.411117553711,
					["w"] = 285.046661376953,
					["x"] = 511.359737503357,
				},
				["Buttons"] = {
					["FileButton"] = false,
					["CloseButton"] = false,
					["LeftButton"] = false,
					["ResetButton"] = false,
					["ConfigButton"] = false,
					["RightButton"] = false,
					["ReportButton"] = false,
				},
			},
			["DetailWindowX"] = 625.607482910156,
			["LastInstanceName"] = "Eye of the Storm",
			["CurDataSet"] = "OverallData",
			["Colors"] = {
				["Window"] = {
					["Title"] = {
						["r"] = 0,
					},
				},
				["Bar"] = {
					["Bar Text"] = {
						["a"] = 1,
					},
					["Total Bar"] = {
						["a"] = 1,
					},
				},
				["Other Windows"] = {
					["Title"] = {
						["r"] = 0,
					},
				},
			},
			["DetailWindowY"] = -259.710144042969,
			["Scaling"] = 1.07,
			["MainWindowWidth"] = 285.046722412109,
			["MainWindowHeight"] = 128.411224365234,
		},
	},
}
