
MADB = {
	["noMMMW"] = false,
	["characters"] = {
	},
	["alwaysShowNudger"] = false,
	["frameListRows"] = 18,
	["profiles"] = {
		["default"] = {
			["name"] = "default",
			["frames"] = {
				["PetFrame"] = {
					["orgPos"] = {
						"TOPLEFT", -- [1]
						"PlayerFrame", -- [2]
						"TOPLEFT", -- [3]
						60.0000038146973, -- [4]
						-99.9999923706055, -- [5]
					},
					["name"] = "PetFrame",
					["pos"] = {
						"BOTTOMLEFT", -- [1]
						"UIParent", -- [2]
						"BOTTOMLEFT", -- [3]
						454, -- [4]
						197.000061035156, -- [5]
					},
				},
			},
		},
	},
	["modifiedFramesOnly"] = true,
	["tooltips"] = true,
	["noBags"] = false,
	["playSound"] = false,
	["closeGUIOnEscape"] = false,
}
