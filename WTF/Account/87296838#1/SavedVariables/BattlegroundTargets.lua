
BattlegroundTargets_Options = {
	["FirstRun"] = true,
	["Friend"] = {
		["ButtonRangeDisplay"] = {
			[40] = 7,
			[10] = 1,
			[15] = 1,
		},
		["ButtonFTargetCountToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["ButtonToTScale"] = {
			[40] = 0.6,
			[10] = 0.8,
			[15] = 0.8,
		},
		["ButtonRangeToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["ButtonAssistToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["LayoutTH"] = {
			[40] = 24,
			[10] = 18,
			[15] = 18,
		},
		["ButtonFlagPosition"] = {
			[40] = 100,
			[10] = 0,
			[15] = 0,
		},
		["ButtonSpecToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonHealthTextToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["ButtonLeaderToggle"] = {
			[40] = true,
			[10] = true,
			[15] = true,
		},
		["ButtonTargetPosition"] = {
			[40] = 85,
			[10] = 100,
			[15] = 100,
		},
		["LayoutSpace"] = {
			[40] = 0,
			[10] = 0,
			[15] = 0,
		},
		["SummaryScale"] = {
			[40] = 0.5,
			[10] = 0.6,
			[15] = 0.6,
		},
		["SummaryToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonToTPosition"] = {
			[40] = 9,
			[10] = 5,
			[15] = 5,
		},
		["ButtonHealthBarToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonToTToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["ButtonSortDetail"] = {
			[40] = 3,
			[10] = 3,
			[15] = 3,
		},
		["ButtonAssistScale"] = {
			[40] = 1,
			[10] = 1.2,
			[15] = 1.2,
		},
		["EnableBracket"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonHeight"] = {
			[40] = 16,
			[10] = 21,
			[15] = 21,
		},
		["ButtonTargetToggle"] = {
			[40] = true,
			[10] = true,
			[15] = true,
		},
		["ButtonRoleToggle"] = {
			[40] = true,
			[10] = true,
			[15] = true,
		},
		["ButtonETargetCountToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonFontNameSize"] = {
			[40] = 10,
			[10] = 12,
			[15] = 12,
		},
		["ButtonWidth"] = {
			[40] = 100,
			[10] = 175,
			[15] = 175,
		},
		["ButtonScale"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonClassToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["ButtonFontNumberStyle"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonFlagToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonFontNumberSize"] = {
			[40] = 10,
			[10] = 10,
			[15] = 10,
		},
		["SummaryPos"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonPvPTrinketToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["ButtonSortBy"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonRealmToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonAssistPosition"] = {
			[40] = 70,
			[10] = 100,
			[15] = 100,
		},
		["ButtonFocusPosition"] = {
			[40] = 55,
			[10] = 0,
			[15] = 0,
		},
		["ButtonFlagScale"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonFocusToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonTargetScale"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonFontNameStyle"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonFocusScale"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
	},
	["FramePosition"] = {
		["FriendMainFrame15"] = {
			["y"] = -117.500427246094,
			["x"] = 31.9990425109863,
			["point"] = "TOPLEFT",
			["s"] = 1,
		},
		["FriendMainFrame10"] = {
			["y"] = -101.500366210938,
			["x"] = 31.9999866485596,
			["point"] = "TOPLEFT",
			["s"] = 1,
		},
		["EnemyMainFrame15"] = {
			["y"] = 166.000213623047,
			["x"] = -207,
			["point"] = "RIGHT",
			["s"] = 1,
		},
		["OptionsFrame"] = {
			["y"] = 17.9996566772461,
			["x"] = -33.0000305175781,
			["point"] = "CENTER",
			["s"] = 1,
		},
		["EnemyMainFrame10"] = {
			["y"] = 166.499420166016,
			["x"] = -206.999877929688,
			["point"] = "RIGHT",
			["s"] = 1,
		},
	},
	["version"] = 27,
	["TransliterationToggle"] = false,
	["MinimapButtonPos"] = -90,
	["MinimapButton"] = false,
	["Enemy"] = {
		["ButtonRangeDisplay"] = {
			[40] = 7,
			[10] = 1,
			[15] = 1,
		},
		["ButtonFTargetCountToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["ButtonToTScale"] = {
			[40] = 0.6,
			[10] = 0.8,
			[15] = 0.8,
		},
		["ButtonRangeToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["ButtonAssistToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["LayoutTH"] = {
			[40] = 24,
			[10] = 18,
			[15] = 18,
		},
		["ButtonFlagPosition"] = {
			[40] = 100,
			[10] = 100,
			[15] = 100,
		},
		["ButtonSpecToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonHealthTextToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["ButtonLeaderToggle"] = {
			[40] = true,
			[10] = true,
			[15] = true,
		},
		["ButtonTargetPosition"] = {
			[40] = 85,
			[10] = 100,
			[15] = 100,
		},
		["LayoutSpace"] = {
			[40] = 0,
			[10] = 0,
			[15] = 0,
		},
		["SummaryScale"] = {
			[40] = 0.5,
			[10] = 0.6,
			[15] = 0.6,
		},
		["SummaryToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonToTPosition"] = {
			[40] = 9,
			[10] = 1,
			[15] = 1,
		},
		["ButtonHealthBarToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonToTToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["ButtonSortDetail"] = {
			[40] = 3,
			[10] = 3,
			[15] = 3,
		},
		["ButtonAssistScale"] = {
			[40] = 1,
			[10] = 1.2,
			[15] = 1.2,
		},
		["EnableBracket"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonHeight"] = {
			[40] = 16,
			[10] = 21,
			[15] = 21,
		},
		["ButtonTargetToggle"] = {
			[40] = true,
			[10] = true,
			[15] = true,
		},
		["ButtonRoleToggle"] = {
			[40] = true,
			[10] = true,
			[15] = true,
		},
		["ButtonETargetCountToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonFontNameSize"] = {
			[40] = 10,
			[10] = 12,
			[15] = 12,
		},
		["ButtonWidth"] = {
			[40] = 100,
			[10] = 175,
			[15] = 175,
		},
		["ButtonScale"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonClassToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["ButtonFontNumberStyle"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonFlagToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonFontNumberSize"] = {
			[40] = 10,
			[10] = 10,
			[15] = 10,
		},
		["SummaryPos"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonPvPTrinketToggle"] = {
			[40] = false,
			[10] = false,
			[15] = false,
		},
		["ButtonSortBy"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonRealmToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonAssistPosition"] = {
			[40] = 70,
			[10] = 100,
			[15] = 100,
		},
		["ButtonFocusPosition"] = {
			[40] = 55,
			[10] = 100,
			[15] = 100,
		},
		["ButtonFlagScale"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonFocusToggle"] = {
			[40] = false,
			[10] = true,
			[15] = true,
		},
		["ButtonTargetScale"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonFontNameStyle"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
		["ButtonFocusScale"] = {
			[40] = 1,
			[10] = 1,
			[15] = 1,
		},
	},
}
